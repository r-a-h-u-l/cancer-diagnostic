# <h1 align="center">Automatic Cancer Diagnostic</h1>
 
<!-- ABOUT THE PROJECT -->
## About The Project
 
The aim of this python script to make model which predict if the person have cancer or not on the basis of data
 
### Built With
 
* [Python](https://www.python.org/)
 
### Installation
 
1. run cancer_diagonostic.py file

### Project Snap

#### output
![Screenshot_2020-12-24_at_9.47.09_AM](/uploads/1e9acc4a1174c9bb31c0137dba7618b2/Screenshot_2020-12-24_at_9.47.09_AM.png)
